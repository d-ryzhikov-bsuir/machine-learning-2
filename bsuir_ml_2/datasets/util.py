from typing import Callable

from tqdm import tqdm


def get_retrive_reporthook(progressbar: tqdm) -> Callable[[int, int, int], None]:
    """Return a function to be used as `urllib.request.urlretrieve` reporthook"""
    def retrieve_reporthook(chunk_num: int, chunk_size: int, total_size: int):
        if total_size != -1:
            progressbar.total = total_size
        progressbar.update(chunk_num * chunk_size - progressbar.n)

    return retrieve_reporthook
