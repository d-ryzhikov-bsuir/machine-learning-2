from typing import Dict

import numpy as np

from ...util import invert_mapping
from .dataset import Dataset
from .read import IMG_SHAPE, IMG_ROW_SHAPE


def row_as_img(row: np.ndarray) -> np.ndarray:
    """Reshape dataset input row into image matrix."""
    assert row.shape == IMG_ROW_SHAPE

    return row.reshape(IMG_SHAPE)


def get_class_examples(ds: Dataset) -> Dict[str, np.ndarray]:
    """Get mapping of class names to class examples."""
    labels_to_names = invert_mapping(ds.class_labels)

    res = {}
    for row, label in zip(ds.x, ds.y):
        name = labels_to_names.pop(label, None)
        if name:
            res[name] = row
    return res
