import json
import pickle
from dataclasses import dataclass
from functools import partial, reduce
from logging import getLogger
from multiprocessing import Pool
from multiprocessing import sharedctypes
from operator import attrgetter, itemgetter, mul
from os import PathLike
from pathlib import Path
from shutil import rmtree
from typing import Callable, Dict, List, NamedTuple, Tuple

import numpy as np
from PIL import Image, UnidentifiedImageError

from ...consts import PICKLE_PROTOCOL
from .dataset import Dataset
from .fetch import SMALL_PATH, LARGE_PATH


IMG_WIDTH = 28
IMG_HEIGHT = 28
IMG_SHAPE = (IMG_HEIGHT, IMG_WIDTH)

IMG_SIZE = reduce(mul, IMG_SHAPE)
IMG_ROW_SHAPE = (IMG_SIZE,)

X_DTYPE = np.dtype(np.uint8)
Y_DTYPE = np.dtype(np.int8)

logger = getLogger(__name__)


class LabeledPath(NamedTuple):
    label: int
    path: Path


_x_arr = None
_y_arr = None


def _reader_init(x_arr: sharedctypes.RawArray, y_arr: sharedctypes.RawArray):
    """Reader process initializer."""
    global _x_arr, _y_arr
    _x_arr = x_arr
    _y_arr = y_arr


def _read_image(i: int, lp: LabeledPath):
    """Read an image into the shared buffer."""
    global _x_arr, _y_arr

    x = np.ctypeslib.as_array(_x_arr)
    y = np.ctypeslib.as_array(_y_arr)

    try:
        with Image.open(lp.path) as f:
            img = np.asarray(f)
    except UnidentifiedImageError:
        logger.debug("Invalid image: %s", lp.path)
        y[i] = -1
    else:
        x[i] = img.reshape(-1)
        y[i] = lp.label


def _create_buf(shape: Tuple[int, ...], dtype: np.dtype) -> sharedctypes.RawValue:
    """Create shared buffer for numpy ndarray"""
    ctypes_type = np.ctypeslib.as_ctypes_type(dtype)
    return sharedctypes.RawValue(reduce(mul, reversed(shape), ctypes_type))


CACHE_FILENAME = ".cache.pickle"


def _get_class_dirs(path: Path) -> List[Path]:
    """Return sorted list of class directories, filtering out cache dir."""
    return sorted(
        (p for p in path.iterdir() if p.name != CACHE_FILENAME), key=attrgetter("name")
    )


def _label_images(path: Path) -> Tuple[List[LabeledPath], Dict[str, int]]:
    """Label images according to the directories they are in.
    Return a list of labeled image paths and dirname to label mapping.
    """
    class_dirs = _get_class_dirs(path)
    labeled_dirs = tuple(enumerate(class_dirs))
    class_labels = {cd.name: label for label, cd in labeled_dirs}
    labeled_paths = [
        LabeledPath(label=label, path=path)
        for label, cd in labeled_dirs
        for path in cd.iterdir()
    ]
    return class_labels, labeled_paths


def _get_valid_rows(x: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    valid_img_indexes = y != -1
    x = x[valid_img_indexes]
    y = y[valid_img_indexes]
    return x, y


def _get_unique_rows(x: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    x, unique_indices = np.unique(x, return_index=True, axis=0)
    y = y[unique_indices]
    return x, y


def _read_raw(path: Path) -> Dataset:
    """Read images into a dataset in parallel."""
    class_labels, labeled_paths = _label_images(path)

    image_count = len(labeled_paths)
    x_buf = _create_buf((image_count, IMG_SIZE), X_DTYPE)
    y_buf = _create_buf((image_count,), Y_DTYPE)

    with Pool(initializer=_reader_init, initargs=(x_buf, y_buf)) as p:
        p.starmap(_read_image, enumerate(labeled_paths), chunksize=1000)

    x = np.ctypeslib.as_array(x_buf)
    y = np.ctypeslib.as_array(y_buf)

    x, y = _get_valid_rows(x, y)
    x, y = _get_unique_rows(x, y)

    return Dataset(x=x, y=y, class_labels=class_labels)


def _read(path: Path, *, force: bool = False) -> Dataset:
    """Read dataset into memory. Use cache if possible."""
    cache_path = path / CACHE_FILENAME
    if cache_path.exists():
        if not force:
            logger.debug("Cache dir '%s' exists. Loading...", cache_path)
            with cache_path.open("rb") as f:
                return pickle.load(f)

        logger.debug("Removing existing cache from %s", cache_path)
        cache_path.unlink()

    logger.debug("Raw data loading from '%s'", path)
    ds = _read_raw(path)

    logger.debug("Store dataset cache: '%s'", cache_path)
    with cache_path.open("wb") as f:
        pickle.dump(ds, f, protocol=PICKLE_PROTOCOL)
    return ds


read_small = partial(_read, SMALL_PATH)
read_large = partial(_read, LARGE_PATH)
