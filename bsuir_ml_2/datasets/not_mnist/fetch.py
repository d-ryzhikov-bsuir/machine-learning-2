import tarfile
from functools import partial
from logging import getLogger
from pathlib import Path
from shutil import rmtree
from urllib.request import urlretrieve

from tqdm import tqdm

from .. import BASE_DATA_DIR
from ..util import get_retrive_reporthook

logger = getLogger(__name__)


def _fetch_and_unpack(url: str, path: Path, *, force: bool = False):
    """Fetch notMNIST dataset. Use `force` param to overwrite existing data."""
    if path.exists():
        if not force:
            logger.debug("Dataset already exists %s", path)
            return

        logger.debug("Removing existing dataset %s", path)
        rmtree(path)

    logger.debug("Downloading dataset from %s", url)
    with tqdm() as progressbar:
        filename, _ = urlretrieve(url, reporthook=get_retrive_reporthook(progressbar))

    logger.debug("Extracting dataset from archive %s", filename)
    if not path.parent.exists():
        path.parent.mkdir()
    with tarfile.open(filename, "r") as tf:
        tf.extractall(path.parent)


SMALL_URL = "https://commondatastorage.googleapis.com/books1000/notMNIST_small.tar.gz"
SMALL_PATH = BASE_DATA_DIR / "notMNIST_small"
fetch_small = partial(_fetch_and_unpack, url=SMALL_URL, path=SMALL_PATH)

LARGE_URL = "https://commondatastorage.googleapis.com/books1000/notMNIST_large.tar.gz"
LARGE_PATH = BASE_DATA_DIR / "notMNIST_large"
fetch_large = partial(_fetch_and_unpack, url=LARGE_URL, path=LARGE_PATH)
