import json
from dataclasses import dataclass
from pathlib import Path
from typing import Dict

import numpy as np
from cached_property import cached_property


@dataclass(frozen=True)
class Dataset:
    x: np.ndarray
    y: np.ndarray
    class_labels: Dict[str, int]

    @cached_property
    def examples_by_class(self) -> Dict[str, np.ndarray]:
        return {
            name: self.x[self.y == label]
            for name, label in self.class_labels.items()
        }
