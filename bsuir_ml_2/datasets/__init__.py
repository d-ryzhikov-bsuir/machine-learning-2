from pathlib import Path

BASE_DIR = Path(__file__).parent
BASE_DATA_DIR = BASE_DIR / ".data"
