import pickle
from pathlib import Path
from sklearn.linear_model import LogisticRegression
from typing import Any, Dict, Callable, Hashable, Mapping, TypeVar

from .consts import PICKLE_PROTOCOL


Key = TypeVar("Key", bound=Hashable)
Value = TypeVar("Value", bound=Hashable)


def invert_mapping(dct: Mapping[Key, Value]) -> Dict[Value, Key]:
    """Map mapping values to keys."""
    return {v: k for k, v in dct.items()}


_cache_dir = Path(__file__).parent / ".cache"


ReturnVal = TypeVar("ReturnVal")


def load_or_call(
    cache_name: str,
    cb: Callable[[], ReturnVal],
    force: bool = False,
) -> ReturnVal:
    """Load result from cache or call the callback and store the result.
    Use `force` arg to force callback execution and overwrite cache."""
    if not _cache_dir.exists():
        _cache_dir.mkdir()

    res_cache_path = _cache_dir / f"{cache_name}.pickle"
    if res_cache_path.exists() and not force:
        with res_cache_path.open("rb") as f:
            return pickle.load(f)

    res = cb()
    with res_cache_path.open("wb") as f:
        pickle.dump(res, f, protocol=PICKLE_PROTOCOL)
    return res
